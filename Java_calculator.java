import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите выражение через пробелы: ");

        String expression = scanner.nextLine();
        String[] items = expression.split(" ");

        double[] numbers = new double[items.length / 2 + 1];
        char[] operators = new char[items.length / 2];

        int j = 0;
        for (int i = 0; i < items.length; i++) {
            if (i % 2 == 0) {
                numbers[j++] = Double.parseDouble(items[i]);
            } else {
                operators[j - 1] = items[i].charAt(0);
            }
        }

        Calculator calculator = new Calculator();
        double result = calculator.evaluate(numbers, operators);

        System.out.println("Результат: " + result);
    }

    public double evaluate(double[] numbers, char[] operators) {
        double result = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            switch (operators[i - 1]) {
                    case '+':
                        result += numbers[i];
                        break;
                    case '-':
                        result -= numbers[i];
                        break;
                    case '*':
                        result *= numbers[i];
                        break;
                    case '/':
                        result /= numbers[i];
                        break;
                }
            }

            return result;
    }
}
