import os

def check_copyright(Java_calculator):
    with open(Java_calculator, 'r') as file:
        first_line = file.readline().strip()

    if first_line.startswith('// Copyright'):
        return True
    else:
        return False

# Перечислите пути к исходным файлам вашего проекта
source_files = ['path/to/Java_calculator.java']

for file in source_files:
    if not check_copyright(file):
        print(f'Копирайт отсутствует в файле: {file}')
        exit(1)
