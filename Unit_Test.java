import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testEvaluate() {
        Calculator calculator = new Calculator();
        double[] numbers = {2.0, 3.0, 4.0};
        char[] operators = {'+', '*'};

        double result = calculator.evaluate(numbers, operators);

        assertEquals(20.0, result, 0.0001);
    }
}
