import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;


public class CalculatorFunctionalTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void GIVEN_small_expression_WHEN_evaluate_THEN_return_correct_result() {
        String input = "1 + 2\n";
        String[] words = input.split(" ");

        Calculator.main(words);

        assertEquals("Результат: 3.0\n", outContent.toString());
    }

    @Test
    public void GIVEN_large_expression_WHEN_evaluate_THEN_return_correct_result() {
        String input = "20 - 10 * 2 + 5 / 2\n";
        String[] words = input.split(" ");

        Calculator.main(words);

        assertEquals("Результат: 5.5\n", outContent.toString());
    }

    @Test
    public void GIVEN_division_by_zero_WHEN_evaluate_THEN_return_error_message() {
        String input = "5 / 0\n";
        String[] words = input.split(" ");

        Calculator.main(words);

        assertEquals("Результат: Infinity\n", outContent.toString());
    }


}

